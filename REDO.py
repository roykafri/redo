import re


def replace_format(old_string):
    return re.sub(r"(\w+.*) - (\d{2}).(\d{2}).(\d{4})", r"\4, \3: \1", old_string)


def main():
    string = open(r"C:\Users\harel\Desktop\file.txt", "r").read()
    #string = 'Bentz - 05/06/1953\r\nBig Bird - 18/04/1884\r\nElmo - 07/01/1934\r\nKipi - 23/10/1186'
    print(string)
    print(replace_format(string))
    f = open(r"C:\Users\harel\Desktop\file.txt", "w")
    f.write(replace_format(string))
    f.close()


if __name__ == '__main__':
    main()